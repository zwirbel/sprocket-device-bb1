#ifndef __DEVICE_CONFIG__
#define __DEVICE_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip config
#define SPROCKET_TYPE       "SPROCKET"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// network config
#define SPROCKET_MODE       1
#define WIFI_CHANNEL        11
#define AP_SSID             "sprocket"
#define AP_PASSWORD         "th3r31sn0sp00n"
#define STATION_SSID        "MyAP"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "bb1"
#define CONNECT_TIMEOUT     10000

// mqtt config
#define MQTT_CLIENT_NAME     "bb1"
#define MQTT_HOST            "192.168.1.2"
#define MQTT_PORT            1883
#define MQTT_ROOT_TOPIC        "wirelos/bb1"

// OTA config
#define OTA_PORT 8266
#define OTA_PASSWORD ""

// WebServer
#define WEB_CONTEXT_PATH "/"
#define WEB_DOC_ROOT "/www"
#define WEB_DEFAULT_FILE "index.html"
#define WEB_PORT 80

/*
connecting Rotary encoder
CLK (A pin) - to any microcontroler intput pin with interrupt
DT (B pin) - to any microcontroler intput pin with interrupt
SW (button pin) - to any microcontroler intput pin
VCC - to microcontroler VCC (then set ROTARY_ENCODER_VCC_PIN -1)
GND - to microcontroler GND
*/
#define ROTARY_ENCODER_A_PIN 35
#define ROTARY_ENCODER_B_PIN 34
#define ROTARY_ENCODER_BUTTON_PIN 21
#define ROTARY_ENCODER_VCC_PIN -1 /*put -1 of Rotary encoder Vcc is connected directly to 3,3V; else you can use declared output pin for powering rotary encoder */
#define ROTARY_ENCODER_UPDATE_INTERVAL 50
#define ROTARY_ENCODER_LOWER_BOUND 0
#define ROTARY_ENCODER_UPPER_BOUND 10
#define ROTARY_ENCODER_CIRCULATE_VALUES true
#define ROTARY_ENCODER_TOPIC "r1"

// Pot
#define POT_THRESHOLD 32
#define POT_POLL_INTERVAL 100
#define POT_PIN_1 36
#define POT_PIN_2 37
#define POT_PIN_3 38
#define POT_PIN_4 39
#define POT_TOPIC_1 "a1"
#define POT_TOPIC_2 "a2"
#define POT_TOPIC_3 "a3"
#define POT_TOPIC_4 "a4"

// switch
#define SWITCH_PIN 17
#define SWITCH_THRESHOLD 1
#define SWITCH_UPDATE_INTERVAL 100
#define SWITCH_TOPIC "d1"
#define SWITCH_PIN_MODE INPUT

// Display
#define DISPLAY_I2C_ADR 0x3c
#define DISPLAY_SDA 4
#define DISPLAY_SCL 15
#define DISPLAY_FONT_SIZE 12
#define DISPLAY_ROWS 4
#define DISPLAY_GEOMETRY 0

#endif