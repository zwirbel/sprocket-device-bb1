#ifndef __ROTARY_PLUGIN_H__
#define __ROTARY_PLUGIN_H__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION 

#include <functional>
#include <vector>
#include <FS.h>
#include <TaskSchedulerDeclarations.h>
#include <Plugin.h>
#include <utils/print.h>
#include <utils/misc.h>
#include "AiEsp32RotaryEncoder.h"

using namespace std;
using namespace std::placeholders;

struct RotaryConfig
{
    int pinA;
    int pinB;
    int pinButton;
    int pinVcc;
    int updateInterval;
    int lowerBound;
    int upperBound;
    bool circulateValues;
    const char* topic;
    //const char* topicButton;
};

class RotaryPlugin : public Plugin {
    public:
        Task inputTask;
        int currentVal = 0;
        ButtonState btnState = BUT_RELEASED;
        AiEsp32RotaryEncoder* rotaryEncoder;
        RotaryConfig config;
        RotaryPlugin(RotaryConfig cfg);
        void activate(Scheduler* userScheduler);
        void checkInput();
};

#endif
