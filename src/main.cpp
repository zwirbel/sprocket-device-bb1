#include <config.h>
#include <WiFiNet.h>
#include <Sprocket.h>
#include <DisplayPlugin.h>
#include <WebServerConfig.h>
#include <WebServerPlugin.h>
#include <WebConfigPlugin.h>
#include <WebApiPlugin.h>
#include <MqttPlugin.h>
#include "inputs/analog/AnalogInputPlugin.h"
#include "inputs/digital/DigitalInputPlugin.h"
#include "RotaryPlugin.h"

WiFiNet *network;
Sprocket *sprocket;
DisplayPlugin *screen;
WebServerPlugin *webServer;
WebConfigPlugin *webConfig;
WebApiPlugin *webApi;
MqttPlugin *mqtt;

RotaryPlugin *r1;
AnalogInputPlugin *a1;
AnalogInputPlugin *a2;
AnalogInputPlugin *a3;
AnalogInputPlugin *a4;
DigitalInputPlugin *d1;

void init()
{
    screen = new DisplayPlugin({DISPLAY_I2C_ADR, DISPLAY_SDA, DISPLAY_SCL, DISPLAY_FONT_SIZE, DISPLAY_ROWS, DISPLAY_GEOMETRY});
    mqtt = new MqttPlugin({MQTT_CLIENT_NAME, MQTT_HOST, MQTT_PORT, MQTT_ROOT_TOPIC}, true, true);
    webServer = new WebServerPlugin({WEB_CONTEXT_PATH, WEB_DOC_ROOT, WEB_DEFAULT_FILE, WEB_PORT});
    webConfig = new WebConfigPlugin(webServer->server);
    webApi = new WebApiPlugin(webServer->server);
    d1 = new DigitalInputPlugin({SWITCH_PIN,SWITCH_THRESHOLD, SWITCH_UPDATE_INTERVAL, SWITCH_TOPIC, SWITCH_PIN_MODE});
    a1 = new AnalogInputPlugin({POT_PIN_1, POT_THRESHOLD, POT_POLL_INTERVAL, POT_TOPIC_1});
    a2 = new AnalogInputPlugin({POT_PIN_2, POT_THRESHOLD, POT_POLL_INTERVAL, POT_TOPIC_2});
    a3 = new AnalogInputPlugin({POT_PIN_3, POT_THRESHOLD, POT_POLL_INTERVAL, POT_TOPIC_3});
    a4 = new AnalogInputPlugin({POT_PIN_4, POT_THRESHOLD, POT_POLL_INTERVAL, POT_TOPIC_4});
    r1 = new RotaryPlugin({ROTARY_ENCODER_A_PIN, ROTARY_ENCODER_B_PIN, ROTARY_ENCODER_BUTTON_PIN, ROTARY_ENCODER_VCC_PIN, ROTARY_ENCODER_UPDATE_INTERVAL, ROTARY_ENCODER_LOWER_BOUND, ROTARY_ENCODER_UPPER_BOUND, ROTARY_ENCODER_CIRCULATE_VALUES, ROTARY_ENCODER_TOPIC});
}

void configure()
{

    screen->display->init();
    screen->display->flipScreenVertically();
    screen->splashScreen("Wibbly", "Wobbly", "0.0.1", "Sprocket");
    r1->rotaryEncoder->setup([] { r1->rotaryEncoder->readEncoder_ISR(); });
    webServer->server->serveStatic("/mqttConfig.json", SPIFFS, "mqttConfig.json");
}

void addPlugins()
{
    sprocket->addPlugin(screen);
    sprocket->addPlugin(webServer);
    sprocket->addPlugin(webConfig);
    sprocket->addPlugin(webApi);
    sprocket->addPlugin(a1);
    sprocket->addPlugin(a2);
    sprocket->addPlugin(a3);
    sprocket->addPlugin(a4);
    sprocket->addPlugin(r1);
    sprocket->addPlugin(d1);
    sprocket->addPlugin(mqtt);
}

String getInfoString()
{
    return "Heap: " + String(ESP.getFreeHeap()) + "\n" +
           "Host: " + String(network->config.hostname) + "\n" +
           "AP: " + String(network->config.stationSSID) + "\n" +
           "IP: " + WiFi.localIP().toString() + "\n" +
           "Gateway: " + WiFi.gatewayIP().toString();
}

void activate() {
    if (network->connect())
    {
        sprocket->activate();
        screen->display->flipScreenVertically();
        sprocket->publish("ui/print", getInfoString());
    }
}

void initScreen(){
    pinMode(16, OUTPUT);
    digitalWrite(16, LOW);  // set GPIO16 low to reset OLED
    delay(50);
    digitalWrite(16, HIGH); // while OLED is running, must set GPIO16 in high
}

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    network = new WiFiNet(SPROCKET_MODE, STATION_SSID, STATION_PASSWORD, AP_SSID, AP_PASSWORD, HOSTNAME, CONNECT_TIMEOUT);
    initScreen();
    init();
    addPlugins();
    configure();
    activate();
}

void loop()
{
    sprocket->loop();
    yield();
}