#include "RotaryPlugin.h"

RotaryPlugin::RotaryPlugin(RotaryConfig cfg)
{
    config = cfg;
    rotaryEncoder = new AiEsp32RotaryEncoder(config.pinA, config.pinB, config.pinButton, config.pinVcc);
    rotaryEncoder->begin();
    rotaryEncoder->setBoundaries(config.lowerBound, config.upperBound, config.circulateValues);
}
void RotaryPlugin::activate(Scheduler *userScheduler)
{
    
    // add update task
    inputTask.set(TASK_MILLISECOND * config.updateInterval, TASK_FOREVER, std::bind(&RotaryPlugin::checkInput, this));
    userScheduler->addTask(inputTask);
    inputTask.enable();

    // add dummy subscription
    subscribe(config.topic, [](String msg){});
    subscribe(String(config.topic) + "/btn", [](String msg){});

    PRINT_MSG(Serial, "PLUGIN", "RotaryPlugin activated");
}

void RotaryPlugin::checkInput()
{
    rotaryEncoder->enable();
    if (rotaryEncoder->encoderChanged())
    {
        int encReading = rotaryEncoder->readEncoder();
        publish(config.topic, String(encReading));
    }
    ButtonState newBtnState = rotaryEncoder->currentButtonState();
    if(newBtnState != btnState){
        btnState = newBtnState;
        publish(String(config.topic) + String("/btn"), String(btnState));
    }
}
